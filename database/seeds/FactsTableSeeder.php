<?php

use App\Fact;
use Illuminate\Database\Seeder;

class FactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Fact::create([
            'fact'       => 'the Moons diameter is 3,476km',
            'min-length' => 3200,
            'max-length' => 3500,
        ]);

        Fact::create([
            'fact'       => 'dolphins can hear underwater sounds from 24km away',
            'min-length' => 23000,
            'max-length' => 25000,
        ]);

        Fact::create([
            'fact'       => 'sharks can sense a drop of blood from 4km away',
            'min-length' => 3500,
            'max-length' => 4500,
        ]);
    }
}
