<?php
namespace Deployer;

require 'recipe/laravel.php';
require 'recipe/npm.php';

// Config
set('default_stage', 'production');

// Project name
set('application', 'api.hoeveelfilestaater.be');

// Project repository
set('repository', 'git@gitlab.com:RoobieBoobieee/api.hoeveelfilestaater.be.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);

// Hosts
host('production')
    ->hostname('hoeveelfilestaater.be')
    ->set('branch', 'master')
    ->set('deploy_path', '~/webroot/api.hoeveelfilestaater.be')
    ->stage('production');

// Tasks
task('build', function () {
    run('cd {{release_path}} && build');
});

task('npm', function () {
    run("cd {{release_path}} && npm run dev");
})->onStage('staging')->desc("Running npm");

task('npm:production', function () {
    run("cd {{release_path}} && npm run production");
})->onStage('production')->desc("Running npm");

// Run npm install
/* after('deploy:update_code', 'npm:install'); */

// Run npm
after('npm:install', 'npm:production');
after('npm:install', 'npm');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');
