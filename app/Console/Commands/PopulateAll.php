<?php

namespace App\Console\Commands;

use DB;
use App\Data;
use Carbon\Carbon;
use App\ProcessedData;
use Illuminate\Console\Command;

class PopulateAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'populate:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('processed_data')->truncate();
        $today    = Carbon::today();
        $day      = Data::first()->recorded;
        $dayCount = 0;

        do {
            $dayCount++;
            $data = Data::whereDate('recorded', $day->toDateString())->get();

            $minimum = 0;
            $average = 0;
            $maximum = 0;
            $count   = 0;

            $processedData = ProcessedData::where('date', $day->toDateString())->get();

            if (! $processedData->isEmpty()) {
                continue;
            }

            foreach ($data as $key => $date) {
                $count++;
                $average += $date->m;
                $minimum = ($date->m < $minimum || $minimum === 0) ? $date->m : $minimum;
                $maximum = ($date->m > $maximum || $maximum === 0) ? $date->m : $maximum;
            }

            if ($count != 0) {
                $average = round($average / $count);
            }
            ProcessedData::create([
                'date' => $day,
                'm'    => $minimum,
                'type' => 'minimum',
            ]);

            ProcessedData::create([
                'date' => $day,
                'm'    => $average,
                'type' => 'average',
            ]);

            ProcessedData::create([
                'date' => $day,
                'm'    => $maximum,
                'type' => 'maximum',
            ]);

            $day->addDay();
        } while ($day < $today);

        $this->info($dayCount . ' days populated.');
    }
}
