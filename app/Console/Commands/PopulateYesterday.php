<?php

namespace App\Console\Commands;

use App\Data;
use Carbon\Carbon;
use App\ProcessedData;
use Illuminate\Console\Command;

class PopulateYesterday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'populate:yesterday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $yesterday = Carbon::yesterday();
        $data      = Data::whereDate('recorded', $yesterday->toDateString())->get();

        $minimum = 0;
        $average = 0;
        $maximum = 0;
        $count   = 0;

        $processedData = ProcessedData::where('date', $yesterday->toDateString())->get();

        if (! $processedData->isEmpty()) {
            return;
        }

        foreach ($data as $key => $date) {
            $count++;
            $average += $date->m;
            $minimum = ($date->m < $minimum || $minimum === 0) ? $date->m : $minimum;
            $maximum = ($date->m > $maximum || $maximum === 0) ? $date->m : $maximum;
        }

        if ($count != 0) {
            $average = round($average / $count);
        }

        ProcessedData::create([
            'date' => $yesterday,
            'm'    => $minimum,
            'type' => 'minimum',
        ]);

        ProcessedData::create([
            'date' => $yesterday,
            'm'    => $average,
            'type' => 'average',
        ]);

        ProcessedData::create([
            'date' => $yesterday,
            'm'    => $maximum,
            'type' => 'maximum',
        ]);
    }
}
