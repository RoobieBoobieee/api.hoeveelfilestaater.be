<?php

namespace App\Console\Commands;

use App\Data;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GetData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $xml = json_decode(file_get_contents('https://vrtnws-api.vrt.be/traffic/teaser'));

        $m        = (int) $xml->teaser->trafficJamLength;
        $recorded = Carbon::parse((string) $xml->issueDate);

        $data = Data::create([
            'm'        => $m,
            'recorded' => $recorded,
        ]);

        $data->save();
    }
}
