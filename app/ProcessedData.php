<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcessedData extends Model
{
    public $table = 'processed_data';

    public $timestamps = false;

    protected $fillable = [
        'date', 'm', 'type',
    ];

    protected $dates = [
        'date',
    ];

    protected $hidden = [
        'id',
    ];
}
