<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fact extends Model
{
    protected $fillable = [
        'fact',
        'min-length',
        'max-length',
    ];

    protected $hidden = [
        'id',
        'min-length',
        'max-length',
        'created_at',
        'updated_at',
    ];
}
