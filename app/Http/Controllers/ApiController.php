<?php

namespace App\Http\Controllers;

use App\Data;
use App\Fact;
use Validator;
use Carbon\Carbon;
use App\ProcessedData;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ApiController extends Controller
{
    public function getWelcomeMessage()
    {
        $today = Carbon::today();
        $data  = Data::whereDate('recorded', $today)->orderBy('id', 'desc')->first();

        if ($data !== null) {
            $cars = $data->m / 6;
            $fact = Fact::create([
                    'fact'       => 'Dat zijn ongeveer ' . floor($cars) . ' auto\'s.',
                    'min-length' => 0,
                    'max-length' => 0,
                ]);
        } else {
            $fact = Fact::create([
                'fact'       => 'Geen data beschikbaar',
                'min-length' => 0,
                'max-length' => 0,
            ]);
        }

        $minimum = Data::whereDate('recorded', $today)->min('m');

        $count       = 0;
        $average     = 0;
        $dataOfToday = Data::whereDate('recorded', $today)->get();
        foreach ($dataOfToday as $key => $date) {
            $count++;
            $average += $date->m;
        }

        if ($count != 0) {
            $average = $average / $count;
        } else {
            $average = 0;
        }

        $maximum = Data::whereDate('recorded', $today)->max('m');

        return [
            'data'    => $data,
            'fact'    => $fact->fact,
            'minimum' => round($minimum / 1000),
            'average' => round($average / 1000),
            'maximum' => round($maximum / 1000),
        ];
    }

    public function getData(Request $request)
    {
        // return $request->all();
        $validator = Validator::make($request->all(), [
            'type'  => Rule::in(['minimum', 'average', 'maximum']),
            'start' => 'date',
            'end'   => 'date',
        ], [
            'type.in' => 'type must be empty or equal to one of the following values: minimum, average (default), maximum',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }

        $labels    = [];
        $data      = [];
        $startDate = $request->input('start', Carbon::today()->subWeek());
        $endDate   = $request->input('end', Carbon::today());

        if ($startDate == $endDate) {
            $records = Data::whereDate('recorded', $startDate)->orderBy('recorded')->get();

            foreach ($records as $key => $record) {
                array_push($labels, $record->recorded->setTimezone('Europe/Brussels')->format('H:i'));
                array_push($data, round($record->m / 1000, 2));
            }
        } else {
            $records = ProcessedData::orderBy('id', 'desc')
                ->where('date', '>=', $startDate)
                ->where('date', '<=', $endDate)
                ->where('type', $request->input('type', 'average'))
                ->get()
                ->reverse();

            foreach ($records as $key => $record) {
                array_push($labels, $record->date->format('D d/m'));
                array_push($data, round($record->m / 1000, 2));
            }
        }

        return [
            'labels' => $labels,
            'data'   => $data,
        ];
    }
}
